I am delighted to announce that as of version 1.0.0 about time will no longer manage its own calendar but will instead rely solely on the excellent SimpleCalendar. There really is no need for multiple modules all trying to provide the same funcitonality and simple calendar is a much nicer module to use to manage calendars.

## BREAKING BREAKING BREAKING
This version of about time 1.0.2 is a huge change and you may need to make some changes.

As of 1.0.0 about time will drop ALL calendar management functionaltiy and insted tightly integrate with Simple Calendar.

As of 1.0.0 about time will rely soley on game.time.worldTime for time updates and will not advance the game clock itself. You can use Simple Calendar/Small time to manage world time updates and those will be recognised by about-time or you can do it yourself or use Small Time.

The sole functionality of about-time going forwards will be to manage events and active effect time outs which is why it was written in the first place, the calendar management was just to support that origianl goal.

### What do I do?

If you have been using about-time to manage your calendar go to simple calendar and from the configure options select **Import Into Simple Calendar** this should bring in you about-time calendar into simple calendar. After that just start using simple calendar to manage your calendar.

If you use calendar-weather updates to your calendar will be pushed to Simple Calendar and you can continue as before. When first running you should import into Simple Calendar (using the simple calendar configure option) thereafter synching should continue. If you make changes to the Simple Calendar calendar you should export into Calendar Weather from the Simple Calendar configure screnn each time you make changes to the Simple Calendar calendar.

Your decision as to whether install about-time/times-up should be based on
1) Do I just want active effects to expire? If yes install times-up.
2) Do I want effects with a duration in rounds/turns to expire correctly? If yes install times-up.
3) Do I use special durations like turn start/turn end, or macro repeat macros? If yes install times-up.
2) Do I want to be able to schedule my own events to occur in the future? If yes install about-time.
3) For a while if you use calendar-weather you will still need to install about-time.

You can have both modules installed without problem.

The about-time api will, eventally, be streamlined to only support the event management functions:
* doAt: unchanged
* doIn: unchanged
* doEvery: unchanged
* doAtEvery: unchanged
* reminderAt: unchanged
* reminderIn: unchanged
* reminderEvery: unchanged
* reminderAtEvery: unchanged
* notifyAt: unchanged
* notifyIn: unchanged
* notifyEvery: unchanged
* notifyAtEvery: unchanged
* clearTimeout: unchanged
* getTimeString: unchanged
* queue: unchanged
* chatQueue: unchanged
* flushQueue: unchanged
* The format for intervals has changed to align with Simple Calendar, in all cases year/month/day/hour/minute/second should be used instead of years/months/days/hours/minutes/seconds. Support for the old form about-time intervals will be removed with foundry 0.9.

The about-time API has had some functions deprecated which will be supported until foundry 0.9.x and some functions removed which are no longer supported. If you enable debug on the about-time settings a host of deprecation warnings will be displayed enabling you to see what functions you need to migrate. (If you use calendar-weather there will be too many deprecation warnings to count so don't worry about them and leave the debug disabled).

The Simple Calendar api provides a nearly direct replacements for the about-time api.
* timestamp (which is an integer) is use to represent a date/time - game.time.worldTime is a timestamp.
* interval ({year: y, month: m, day: d, hour: h, minute:m, second: s }) replaces DTMod.
* You can add intervals to timestamps. SimpleCalendar.api.timestampPlusInterval(game.time.worldTime, {hour: 1}), returns a timestamp one hour from the current game.time.worldTime.
* You can convert timestamps to intervals, SimpleCalendar.api.timestampToDate(game.time.worldTime) returns the current game time as an interval, augmented with year names and days of week information.
* You can set the game data/time via SimpleCalendar.api.setDate({year: y, month: m, day: d, hour: h, hour: h, minute: m, second: s). Omitted fields will be replaced with the current game time values.

## Deprecated/Removed api calls
* isMaster:  deprecated
* isRunning deprecated
    use SimpleCalendar.api.clockStatus().started instead
* getTime: deprecated use
```
    const datetime = window.SimpleCalendar.api.timestampToDate(game.time.worldTime);
    return `${datetime.hour}:${datetime.minute}:${datetime.second}`
```
* DTM: deprecated: use {year: y, .....} instead
* DTC: deprecated, use timestampPlusInterval instead.
* DT: deprecated, use game.time.worldTime/timestamp instead.
* DMf: no longer required: use {hour: H minute:M instead}
* DTf: no longer required.
* DTNow: dprecated: use game.time.worldTime instead.
* calendars: removed
* _notifyEvent: 
* startRunning: deprecated - use SimpleCalendar.api.startClock()
* stopRunning: deprecated - use SimpleCalendar.api.stopClock()
* advanceClock: deprecated - use game.time.advance(seconds)/game.settings.set("core", "time", timestamp)
* advanceTime: deprecated - use game.time.advance(seconds)/game.settings.set("core", "time", timestamp)
* setClock: deprecated: - use SimpleCalendar.api.setDate(date/time)
* setTime: deprecated: use - game.settings.set("core", "time", timestamp)
* setAbsolute: deprecated - use SimpleCalendar.api.setDate(date)
* setDateTime: deprecated -  use SimpleCalendar.api.setDate(date/time)
* reset: removed
* resetCombats: removed
* status: deprecated - use SimpleCalendar.api.clockStatus()
* pc: removed
* showClock: deprecated - use SimpleCalendar.show()
* showCalendar: deprecated - use SimpleCalendar.show()
* CountDown: removed - peding a rewrite
* RealTimeCountDown: removed - pending a rewrite.

About-time uses the system game.time.worldTime for storing about-time game time.
**Breaking** Time update hook call is now updateWorldTime and passes the time in seconds.  
**FUTURE Breaking** Support for function calls as scheduled items will be removed. Either use a macro or trigger an event with the data you need and Hook the update.  
If the setting settings-per-round is set to 0, about-time will use the system default CONFIG.time.roundTime, otherwise it will overwrite CONFIG.time.roundTime with whatever is set by the user.
**game.Gametime.DTNow().longDateExtended()** returns the current date/time plus the current dow string and monthstring for you to format to your hearts content.  

### About-time
A module to support a game time clock in Foundry VTT. Arbitrary calendars are supported Gregorian and Greyhawk supplied but it is easy to add your own. See notes at the end. Please have a look at Gettingstarted.md it has more information.

DateTime Arithmentic is supported allowing calculations like DateTime.Now().add({days:1}) (increments can be negative, but must be integers)  

DateTime represents a date and time comprising ({years, months, days, hours, minutes, seconds}) and repsents an actual date and time.  
DTMod is a class for representing time intervals, game.Gametime.DTMod.create({years, months, days, hours. minutes, seconds}) only represents an interval to be applied to a DateTime. Typically you would do things like DateTime.now().add({days: 5})  

See also gettingStarted.md  
The gametime clock can bet set/advanced as you wish. The clock can also be set to run in real time at a multiple/fraction of real world time. The game clock is snchronised betwee all clients and time updates are broadcast to each client. You can register for them via Hooks.on("clockUpdate").

The real use for the clock is having events fire at predefined times or intervals.
You can call arbitrary code or macros based off the game time clock. Allowing things like 
```game.Gametime.doEvery({hours:24}, "Eat Food")```

There are three event triggering calls
* doXXX call a javascript function or execute a macro.
* reminderXXX send a message to the console.
* notifyXXX(eventName, args). Notify all clients via Hooks.call("about-time.eventTrigger", name, args).

XXX can be one of At, In, Every.
* So doAt(DateTime,....) runs the handler at the given clock time.
* doIn(DTMod,....)  runs the handler after DTMod time from now.
* doEvery(DTMod) rus the event every time DTMod time passes.

When combat starts the clock enters a special mode where game time advances 6 seconds (configurable) at the end of each turn. This allows for timed effects/conditions, e.g.
when blessed the macro could look like:
```
DynamicItems.toggleEffectActive(actor, "Bless", true);
game.Gametime.doIn(game.Gametime.DMf{minutes:1}, DynamicItems.toggleEffectsActive, actor, "Bless", false) 
game.Gametime.doIn({minutes: 1}, ....)
``` 
so that bless will finish after 60 seconds.

When combat ends the clock reverts to it's previous mode.

There are gametime "Events" which are specified as ET.notifyEvery({hours: 1}, "There is a wandering monster in the vicinity", ...args). This will fire the hook "eventTrigger" on all connected clients. It can be waited for via Hooks.on("eventTrigger", (...args) => {})

The event queue, from where handlers fire is per client and persistent so whenever they start up their queue will be active.  
The core classes are  
DTCalc stores infromation about the calendar, exposed as game.Gamtime.DTCalc or the global DTC.
DTMod represents time interals, exposed as game.Gametime.DTMod or the global DM.  
DateTime represents a DateTime in the current calendar, exposed as game.Gametime.DateTime or the global DT.  
ElapsedTime represents the per client event queue, exposed as game.Gametime.ElapsedTime or the global ET.
Pseudoclock represents the game clock. Not exposed directly, but has helper methods exposed in ElapsedTime. The clock and queue states are persistent across game restarts.  

## What time is it?
If you are using about-time without calendar weather you can display a simple game clock with
```
Gametime.showClock();
Gametime.showCalendar();
```
This will display the current data/time or the current date. For the GM there are some buttons to advance the world time. If the display is red world time is paused, clicking on the time display will start the world clock, unless the game is paused.
For showClock() (only) pressing the shift key deducts the specified time from the clock.

```
Gametime.CountDown.startTimer(duration, forceRealTime);
Gametime.CountDown.startTimer({minutes: 10}, false);
Gametime.CountDown.startTimerAllPlayers({minutes: 10}, false);
Gametime.CountDown.showTimer();
Gametime.RealTimeCountDown.startTimer({minutes:10});
Gametime.RealTimeCountDown.startTimerAllPlayers({minutes:10});
Gametime.RealTimeCountDown.showTimer();

```
You can create a countdown timer whose duration is specified in the first argument and force the world clock to real time if the second parameter is true.  
startTimer creates a timer on the current client.  
startTimerAllPlayers creates and displays a timer on all connected clients (GM only).  
showTimer redisplays the timer if the timer window was closed.
RealTimeCountdown creates a real time (not game time) countdown timer. Game time settings are not modified.

## Usage
When the module is enabld a psuedo clock is started on the GM's client. Each other client recieves time updates from the GM's client  whnever the GM changes the clock.

Why should I care?  
You can now accurtaley track game time in whatever way you want.  
You can trigger events to occur at specified times. The event queue is local to each client.  
In combat the pseudo clock advances by 6 seconds per round so you can implement time effects which last for say 10 rounds.  
You can track elapsed game time with macros like   
```game.Gametime.advanceTime(game.Gametime.DMf("hours: 1}))```

** At present the interface is programatic only **
Access these via game.Gametime.XXXXX

* doEvery(DM{days:1}, () => console.log("another day another dollar"))  
          every day run the handler.
* doAt({hours:1}, "My Macro"))  
          run a macro in 1 hour of gametime
* reminderAt({minutes: 10}, "Check for wandering monsters"}  
          send a message to the console.
* reminderEvery({minutes: 10}, "Check for wandering monsters"}  
    do a reminder every 10 minutes.  
  Each of the above return an id which can be used to cancel the event
* clearTimeout(eventID)

* currentTime() return the current time in seconds
* currentTimeString() return the current time in the form "dd hh:mm:ss"

* game.Gametime.isMaster() is the cleint the master time server
* game.Gametime.isRunning() is the pseudo clock running in realtime
* game.Gametime.doAt(): At the specified time do the handler/macro at a specified time doAt(datetime, handler, arguments) or doAt(dateTime, macro)
* game.Gametime.doIn(): Execute the specified handler/macro in the spericifed interval, i.e. doIn({mintues: 30}, ....)
* game.Gametime.doEvery(): ElapsedTime.doEvery,
* game.Gametime.reminderAt(): At the specified time log the message to the console reminderAt(datetime, "reminder text", ...args)
* game.Gametime.reminderIn(): After the specified interval send the reminder to the console
* game.Gametime.reminderEvery(): Every DTMod inteval send the specified message to theconsole
* game.Gametime.notifyAt(): notifyAt(DateTime, "event name", ...args) At the specified time call Hooks. callAll("eventTrigger", eventName, ...args)
* game.Gametime.notifyIn(): After DTMod interval notify all cleints
* game.Gametime.notifyEvery(): Every DTMod interval nofity all clients.
* game.Gametime.clearTimeout:() Each doXXX, remindXXX, notifyXXX registration returns an ID. clearTimeout(id) can be used to cancel the timeout
* game.Gametime.getTimeString(): Return the current gameTime as a string HH:MM:SS
* game.Gametime.getTime(): return a DTMod with the current time
* game.Gametime.queue(): show the current event queue
* chatQueue({showArgs = false, showUid = false, showDate = false}) - show the queue to the chat log. parameters are optional and allow a little config of the output
* game.Gametime.ElapsedTime the ElapsedTime singleton
* game.Gametime.DTM(): The DTMod class
* game.Gametime.DTC(): The DTCalc class
* game.Gametime.DT(): The DateTime calss
* game.Gametime.DMf(): create a new DTMod({years:y, months:m, days:d, hours:h, minutes:m, seconds:s})
* game.Gametime.DTf(): create the date time corresponding to {years:y, months:m, days:d, hours:h, minutes:m, seconds:s}. If years is omitted it defaults to 1970 or all parameters omitted defaults to 1/1/1970, 00:00:00.
* game.Gametime.DTNow() current gametime
* game.Gametime.DTf({years: 1900, months: 5, days: 3}).longDate() yields {date: "Sunday June 04 1900", time: "00:00:00"}
and game.Gametime.DTf({years: 1900, months: 5, days: 3}).shortDate() yields {date: "1900/06/04", time: "00:00:00"}


On the GM's client the following can be used
* game.Gametime.startRunning() start the pseudo real time clock
* game.Gametime.stopRunning() stop the pseudo real time clock
* game.Gametime.advanceClock(timeInSeconds)
*
* game.Gametime.advanceTime({days: d, hours: h, minutes: m, seconds: s}) advance the clock by the specified amount.
* game.Gametime.setTime(DateTime): set the current time to the specified date time
* game.Gametime.setClock(timeInSeconds) set the clock to timeInSeconds

** DTCalc calendar format **
Years are 1 based and months/days are 0 based. So February 23rd 1970 is represented as 1970/1/22

const  GregorianCalendar = {  
  // month lengths in days - first number is non-leap year, second is leapy year  
  "month_len": {  
      "January": {days: [31,31], intercalary: false},  
      "February": {days:[28, 29]},  
      "March": {days:[31,31]},  
      "April": {days:[30,30]},  
      "May": {days:[31,31]},  
      "June": {days:[30,30]},  
      "July": {days:[31,31]},
      "August": {days:[31,31]},  
      "September": {days:[30,30]},  
      "October": {days:[31,31  
      "November": {days:[30,30]},  
      "December": {days:[31,31]},  
  },  
  // a function to return the number of leap years from 0 to the specified year.   
  "leap_year_rule": (year) => Math.floor(year / 4) - Math.floor(year / 100) + Math.floor(year / 400),  
  // names of the days of the week. It is assumed weeklengths don't change  
  "weekdays": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],  
  // year when the clock starts and time is recorded as seconds from this 1/1/clock_start_year 00:00:00. If is set to 1970 as a unix joke. you can set it to 0.  
  "clock_start_year": 1970,  
  // day of the week of 0/0/0 00:00:00  
  "first_day": 6,  
  "notes": {},  
  "hours_per_day": 24,  
  "seconds_per_minute": 60,  
  "minutes_per_hour": 60,  
  // Is there a year  
  "has_year_0": false,
  "yearNames": ["chicken", "rat", "tiger"], // Optional named years - can be omitted.
  "NamedYears" :{10: "special name", 20: "another special name"} // give names to specific years - can be omitted.
}  

Initialise DTCalc with the new calendar via DTCalc._createFromData(GregorianCalendar). All module intialisation fires on the setup/ready hooks so you can set the calendar on the init hook and all should be fine.